ARG RUBY_IMAGE=

FROM ${RUBY_IMAGE}

ARG GITALY_SERVER_VERSION
ARG GITLAB_USER=git
ARG DNF_OPTS
ARG UID=1000

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitaly" \
      name="Gitaly" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITALY_SERVER_VERSION} \
      release=${GITALY_SERVER_VERSION} \
      summary="Gitaly is a Git RPC service for handling all the git calls made by GitLab." \
      description="Gitaly is a Git RPC service for handling all the git calls made by GitLab."

ADD gitaly.tar.gz /
ADD gitlab-logger.tar.gz /usr/local/bin

COPY scripts/ /scripts/

RUN dnf ${DNF_OPTS} install -by --nodocs procps net-tools libicu openssh-clients \
    && dnf clean all \
    && rm -r /var/cache/dnf \
    && rm -f /usr/local/tmp/openssh-server-*.rpm /usr/libexec/openssh/ssh-keysign \
    && adduser -u ${UID} -m ${GITLAB_USER} \
    && mkdir -p /etc/gitaly /var/log/gitaly /home/${GITLAB_USER}/repositories /srv/gitlab-shell \
    && touch /var/log/gitaly/gitaly.log \
    && touch /var/log/gitaly/gitlab-shell.log \
    && cd /srv/gitaly-ruby \
    && bundle binstubs --all \
    && chown -R ${UID}:0 \
      /scripts \
      /etc/gitaly \
      /var/log/gitaly \
      /home/${GITLAB_USER}/repositories \
      /srv/gitaly-ruby \
      /srv/gitlab-shell \
      /home/${GITLAB_USER} \
    && chmod -R g=u \
      /scripts \
      /etc/gitaly \
      /var/log/gitaly \
      /home/${GITLAB_USER}/repositories \
      /srv/gitaly-ruby \
      /srv/gitlab-shell \
      /home/${GITLAB_USER}

# add default configuration template
COPY --chown=${UID}:0 config.toml /etc/gitaly/config.toml.erb

USER ${UID}

ENV PATH "$PATH:/srv/gitaly-ruby/bin"
ENV CONFIG_TEMPLATE_DIRECTORY=/etc/gitaly

CMD ["/scripts/process-wrapper"]

VOLUME /var/log/gitaly

HEALTHCHECK --interval=30s --timeout=10s --retries=5 CMD /scripts/healthcheck
