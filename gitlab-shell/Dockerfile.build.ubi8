ARG BUILD_IMAGE=

FROM ${BUILD_IMAGE}

ARG NAMESPACE=gitlab-org
ARG PROJECT=gitlab-shell
ARG VERSION=master
ARG API_URL=
ARG API_TOKEN=
ARG DNF_OPTS
ARG FIPS_MODE=0

ADD gitlab-ruby.tar.gz /
ADD gitlab-go.tar.gz /

ENV PRIVATE_TOKEN=${API_TOKEN}

RUN dnf ${DNF_OPTS} install -by --nodocs git \
    && mkdir -p /assets/usr/local/tmp \
    && mkdir -p /assets/srv/gitlab-shell \
    && ln -sf /usr/local/go/bin/* /usr/local/bin \
    && /gitlab-fetch \
        "${API_URL}" \
        "${NAMESPACE}" \
        "${PROJECT}" \
        "${VERSION}" \
    && cd ${PROJECT}-${VERSION} \
    && FIPS_MODE=${FIPS_MODE} make build \
    && rm -rf go/ go_build/ spec/ internal/testhelper/testdata/ \
    && mkdir /assets/licenses && cp LICENSE /assets/licenses/GitLab.txt \
    && export PREFIX=/assets/srv/gitlab-shell \
    && mkdir -p ${PREFIX}/bin \
    && install -m755 bin/check ${PREFIX}/bin/check \
    && install -m755 bin/gitlab-shell ${PREFIX}/bin/gitlab-shell \
    && install -m755 bin/gitlab-shell-authorized-keys-check ${PREFIX}/bin/gitlab-shell-authorized-keys-check \
    && install -m755 bin/gitlab-shell-authorized-principals-check ${PREFIX}/bin/gitlab-shell-authorized-principals-check \
    && install -m755 bin/gitlab-sshd ${PREFIX}/bin/gitlab-sshd \
    && cp LICENSE VERSION ${PREFIX}/
